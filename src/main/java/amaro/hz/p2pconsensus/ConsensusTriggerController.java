package amaro.hz.p2pconsensus;

import amaro.hz.p2pconsensus.service.api.ConsensusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@RestController
public class ConsensusTriggerController {

    private final ConsensusService consensusService;

    @Autowired
    public ConsensusTriggerController(ConsensusService consensusService) {
        this.consensusService = consensusService;
    }

    @GetMapping(path = "/consensus", produces = TEXT_HTML_VALUE)
    public DeferredResult<String> achieveTerritoryConsensus(@RequestParam String territories) {
        List<String> territoryValues = stream(territories.toUpperCase().split(",")).collect(toList());

        DeferredResult<String> result = new DeferredResult<>();
        consensusService.distributeTerritories(territoryValues).whenComplete((territoryMapping, error) -> {
            if (error != null) {
                result.setErrorResult(error);
                return;
            }

            String resultValue = territoryMapping
                    .entrySet()
                    .stream()
                    .map(territory -> territory.getKey() + " : " + territory.getValue())
                    .collect(joining("<br>"));

            result.setResult(resultValue);
        });

        return result;
    }
}
