package amaro.hz.p2pconsensus.service.api;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface ConsensusService {

    /**
     * Submits a task to the cluster in order to achieve consensus on the territory values.
     *
     * @return the mapping between memberUuid <-> assigned territory
     */
    CompletableFuture<Map<String, String>> distributeTerritories(List<String> territories);
}
