package amaro.hz.p2pconsensus.service;

import amaro.hz.p2pconsensus.service.api.ConsensusService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.supplyAsync;

@Service
public class ConsensusServiceImpl implements ConsensusService {
    static final String TERRITORY_OWNERS = "territory_owners";
    private static final String CONSENSUS_EXECUTOR = "territory_consensus_executor";

    private final HazelcastInstance hazelcastInstance;

    @Autowired
    public ConsensusServiceImpl(HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
    }

    @Override
    public CompletableFuture<Map<String, String>> distributeTerritories(List<String> territories) {
        hazelcastInstance.getMap(TERRITORY_OWNERS).clear();

        return supplyAsync(() -> {
            TerritoryConsensusTask task = prepareConsensus(territories);
            hazelcastInstance.getExecutorService(CONSENSUS_EXECUTOR).executeOnAllMembers(task);

            IMap<String, String> territoriesPerMember = hazelcastInstance.getMap(TERRITORY_OWNERS);
            while (territoriesPerMember.size() < territories.size()) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    throw new IllegalStateException("Error while waiting for consensus to be achieved", e);
                }
            }

            return territoriesPerMember;
        });
    }

    private TerritoryConsensusTask prepareConsensus(List<String> territories) {
        double territoriesPerMember = territories.size() / (double) hazelcastInstance.getCluster().getMembers().size();
        return new TerritoryConsensusTask(territories, territoriesPerMember <= 1 ? 1 : (int) Math.ceil(territoriesPerMember));
    }
}
