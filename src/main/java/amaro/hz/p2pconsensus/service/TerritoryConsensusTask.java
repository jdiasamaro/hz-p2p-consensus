package amaro.hz.p2pconsensus.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.HazelcastInstanceAware;
import com.hazelcast.core.IMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static amaro.hz.p2pconsensus.service.ConsensusServiceImpl.TERRITORY_OWNERS;

public class TerritoryConsensusTask implements Runnable, Serializable, HazelcastInstanceAware {

    private final int amountOfTerritoriesToKeep;
    private final List<String> territories;

    private IMap<String, String> territoriesPerMember;
    private String memberId;

    TerritoryConsensusTask(List<String> territories, int amountOfTerritoriesToKeep) {
        this.amountOfTerritoriesToKeep = amountOfTerritoriesToKeep;
        this.territories = territories;
    }

    @Override
    public void setHazelcastInstance(HazelcastInstance hazelcastInstance) {
        this.territoriesPerMember = hazelcastInstance.getMap(TERRITORY_OWNERS);
        this.memberId = hazelcastInstance.getLocalEndpoint().getUuid();
    }

    @Override
    public void run() {
        List<String> ownedTerritories = new ArrayList<>();

        try {
            while (ownedTerritories.size() < amountOfTerritoriesToKeep && !territories.isEmpty()) {
                String tentativeTerritory = territories.remove(0);
                boolean territoryIsLocked = territoriesPerMember.tryLock(tentativeTerritory);

                if (territoryIsLocked) {
                    territoriesPerMember.set(tentativeTerritory, memberId);
                    ownedTerritories.add(tentativeTerritory);
                }
            }
        } finally {
            ownedTerritories.forEach(territoriesPerMember::unlock);
        }
    }
}
