package amaro.hz;

import amaro.hz.config.HazelcastConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = HazelcastConfiguration.class)
public class HzP2pConsensusApplication {

    public static void main(String[] args) {
        SpringApplication.run(HzP2pConsensusApplication.class, args);
    }
}
