package amaro.hz.semantics.domain;

import java.util.UUID;

public class Operation {

    public enum OperationType {
        DEPOSIT,
        WITHDRAW,
        GET_VALUE
    }

    private UUID id;
    private Integer parameter;
    private OperationType type;

    private Operation() { }

    public UUID getId() {
        return id;
    }

    private void setId(UUID id) {
        this.id = id;
    }

    public Integer getParameter() {
        return parameter;
    }

    public OperationType getType() {
        return type;
    }

    public static final class OperationBuilder {
        private UUID id;
        private Integer parameter;
        private OperationType type;

        private OperationBuilder() {
        }

        public static OperationBuilder anOperation() {
            return new OperationBuilder();
        }

        public OperationBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public OperationBuilder withParameter(Integer parameter) {
            this.parameter = parameter;
            return this;
        }

        public OperationBuilder withType(OperationType type) {
            this.type = type;
            return this;
        }

        public Operation build() {
            Operation operation = new Operation();
            operation.parameter = this.parameter;
            operation.id = this.id;
            operation.type = this.type;
            return operation;
        }
    }
}
