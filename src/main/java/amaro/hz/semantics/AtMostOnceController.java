package amaro.hz.semantics;

import amaro.hz.semantics.service.api.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@RestController
public class AtMostOnceController {

    private final BankService bankService;

    @Autowired
    public AtMostOnceController(BankService bankService) {
        this.bankService = bankService;
    }

    @PostMapping(path = "/deposit", produces = TEXT_HTML_VALUE)
    public DeferredResult<String> deposit(@RequestParam int value) {
        DeferredResult<String> result = new DeferredResult<>();

        bankService.deposit(value).whenComplete((response, error) -> {
            if (error != null) {
                result.setErrorResult(error);
                return;
            }

            result.setResult("Deposit was successful. Current account value: " + response);
        });

        return result;
    }

    @PostMapping(path = "/withdraw", produces = TEXT_HTML_VALUE)
    public DeferredResult<String> withdraw(@RequestParam int value) {
        DeferredResult<String> result = new DeferredResult<>();

        bankService.withdraw(value).whenComplete((response, error) -> {
            if (error != null) {
                result.setErrorResult(error);
                return;
            }

            result.setResult("Withdrawal was successful. Current account value: " + response);
        });

        return result;
    }

    @GetMapping(path = "/account", produces = TEXT_HTML_VALUE)
    public DeferredResult<String> getValue() {
        DeferredResult<String> result = new DeferredResult<>();

        bankService.getValue().whenComplete((response, error) -> {
            if (error != null) {
                result.setErrorResult(error);
                return;
            }

            result.setResult("Current account value: " + response);
        });

        return result;
    }
}
