package amaro.hz.semantics.service.api;

import java.util.concurrent.CompletableFuture;

public interface BankService {

    /**
     * Increments the account with the requested value and returns the account value after the deposit
     */
    CompletableFuture<Integer> deposit(int value);

    /**
     * Depletes the account with the requested value and returns the account value after the withdrawal
     */
    CompletableFuture<Integer> withdraw(int value);

    /**
     * @return the current value of the account
     */
    CompletableFuture<Integer> getValue();
}
