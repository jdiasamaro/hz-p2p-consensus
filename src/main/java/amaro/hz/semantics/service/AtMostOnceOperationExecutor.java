package amaro.hz.semantics.service;

import amaro.hz.semantics.domain.Operation;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

import static amaro.hz.config.HazelcastConfiguration.ACCOUNT_BALANCE_ID;
import static amaro.hz.config.HazelcastConfiguration.OPERATIONS_TOPIC_ID;
import static amaro.hz.config.HazelcastConfiguration.OPERATION_RESULTS_MAP_ID;

@Component
public class AtMostOnceOperationExecutor implements MessageListener<Operation> {

    private final IMap<UUID, Integer> operationResults;
    private final ITopic<Operation> operationsTopic;
    private final IAtomicLong accountBalance;

    @Autowired
    public AtMostOnceOperationExecutor(
            @Qualifier(OPERATION_RESULTS_MAP_ID) IMap<UUID, Integer> operationResults,
            @Qualifier(OPERATIONS_TOPIC_ID) ITopic<Operation> operationsTopic,
            @Qualifier(ACCOUNT_BALANCE_ID) IAtomicLong accountBalance) {
        this.operationResults = operationResults;
        this.operationsTopic = operationsTopic;
        this.accountBalance = accountBalance;
    }

    @PostConstruct
    public void registerOperationListener() {
        operationsTopic.addMessageListener(this);
    }

    @Override
    public void onMessage(Message<Operation> message) {
        UUID operationId = message.getMessageObject().getId();
        boolean operationLocked = operationResults.tryLock(operationId);

        long value;
        if (operationLocked) {
            switch (message.getMessageObject().getType()) {
                case DEPOSIT:
                    value = accountBalance.addAndGet(message.getMessageObject().getParameter());
                    break;
                case WITHDRAW:
                    value = accountBalance.addAndGet(-1 * message.getMessageObject().getParameter());
                    break;
                default:
                    value = accountBalance.get();
            }

            operationResults.set(operationId, (int) value);
        }
    }
}
