package amaro.hz.semantics.service;

import amaro.hz.semantics.domain.Operation;
import amaro.hz.semantics.domain.Operation.OperationType;
import amaro.hz.semantics.service.api.BankService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Member;
import com.hazelcast.map.listener.EntryAddedListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import static amaro.hz.semantics.domain.Operation.OperationBuilder.anOperation;
import static amaro.hz.semantics.domain.Operation.OperationType.DEPOSIT;
import static amaro.hz.semantics.domain.Operation.OperationType.GET_VALUE;
import static amaro.hz.semantics.domain.Operation.OperationType.WITHDRAW;
import static amaro.hz.config.HazelcastConfiguration.OPERATION_RESULTS_MAP_ID;
import static java.util.stream.Collectors.toSet;

@Service
public class BankServiceImpl implements BankService {

    private final Set<String> clusterMembers;
    private final HazelcastInstance hazelcast;
    private final IMap<UUID, Integer> operationResults;

    @Autowired
    public BankServiceImpl(HazelcastInstance hazelcast, @Qualifier(OPERATION_RESULTS_MAP_ID) IMap<UUID, Integer> operationResults) {
        this.hazelcast = hazelcast;
        this.operationResults = operationResults;
        this.clusterMembers = hazelcast.getCluster().getMembers().stream().map(Member::getUuid).collect(toSet());
    }

    @Override
    public CompletableFuture<Integer> deposit(int value) {
        return publishOperation(DEPOSIT, value);
    }

    @Override
    public CompletableFuture<Integer> withdraw(int value) {
        return publishOperation(WITHDRAW, value);
    }

    @Override
    public CompletableFuture<Integer> getValue() {
        return publishOperation(GET_VALUE, null);
    }

    private CompletableFuture<Integer> publishOperation(OperationType type, Integer parameter) {
        CompletableFuture<Integer> futureResult = new CompletableFuture<>();

        UUID operationId = UUID.randomUUID();
        operationResults.addEntryListener(onEntryAdded(futureResult::complete), operationId, true);

        clusterMembers.parallelStream().forEach(member -> {
            ITopic<Operation> memberTopic = hazelcast.getTopic(member);
            memberTopic.publish(anOperation()
                    .withParameter(parameter)
                    .withId(operationId)
                    .withType(type)
                    .build());
        });

        return futureResult;
    }

    private EntryAddedListener<UUID, Integer> onEntryAdded(Consumer<Integer> consumer) {
        return event -> consumer.accept(event.getMergingValue());
    }
}
