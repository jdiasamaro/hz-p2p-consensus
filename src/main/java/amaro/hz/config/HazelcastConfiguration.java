package amaro.hz.config;

import amaro.hz.semantics.domain.Operation;
import com.hazelcast.config.Config;
import com.hazelcast.config.cp.CPSubsystemConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicLong;
import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class HazelcastConfiguration {
    public static final String OPERATION_RESULTS_MAP_ID = "operations";
    public static final String OPERATIONS_TOPIC_ID = "operations";
    public static final String ACCOUNT_BALANCE_ID = "account";

    @Bean
    public HazelcastInstance hazelcastInstance() {
        Config config = new Config();
        config.setCPSubsystemConfig(new CPSubsystemConfig().setCPMemberCount(3));
        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean(name = OPERATIONS_TOPIC_ID)
    public ITopic<Operation> messageTopic(HazelcastInstance hazelcastInstance) {
        return hazelcastInstance.getTopic(hazelcastInstance.getLocalEndpoint().getUuid());
    }

    @Bean(name = OPERATION_RESULTS_MAP_ID)
    public IMap<UUID, Integer> operationResults(HazelcastInstance hazelcastInstance) {
        return hazelcastInstance.getMap(OPERATION_RESULTS_MAP_ID);
    }

    @Bean(name = ACCOUNT_BALANCE_ID)
    public IAtomicLong accountBalance(HazelcastInstance hazelcastInstance) {
        return hazelcastInstance.getCPSubsystem().getAtomicLong(ACCOUNT_BALANCE_ID);
    }
}
