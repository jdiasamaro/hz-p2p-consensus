# Hazelcast Distributed Decision Making

A simple project to demo Hazelcast capabilities as a way to achieve distributed consensus
among cluster members in order to obtain ownership of a unique set of values
